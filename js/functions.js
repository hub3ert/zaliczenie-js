
function formValidate () {
    $(document).on('change', 'input', function () {
        let _ = $(this);
        let validate = _.attr('data-validate');

        if (_.val() == '') {
            _.addClass("error");
            _.attr('data-empty', 'true');
        } else {
            _.removeClass("error");
            _.attr('data-empty', 'false');
        }

        if (_.attr('type') == 'radio' && _.is(':checked')) {
            $('[data-empty="true"][type="radio"]').attr('data-empty', "false");
            if (_.val() == 0) {
                $('[data-visible]').parent().parent().removeClass('hidden');
                $('[data-visible]').attr('data-empty', "true");
            } else {
                $('[data-visible]').parent().parent().addClass('hidden');
                $('[data-visible]').attr('data-empty', "false");
            }
        }

        switch (validate) {
            case "zip":
                regexp = /^[0-9]{2}-[0-9]{3}$/;
                if (regexp.test(_.val())) {
                    _.removeClass("error");
                    _.addClass("success");
                } else {
                    _.removeClass("success");
                    _.addClass("error");
                }
                break;
            case "email":
                regexp = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
                if (regexp.test(_.val())) {
                    _.removeClass("error");
                    _.addClass("success");
                } else {
                    _.removeClass("success");
                    _.addClass("error");
                }
                break;
            default:
                break;
        }
        let empty = $('[data-empty="true"]');
        if (empty.length > 0) {
            $('[data-msg]').text('Wypełnij wszystkie pola');
            $('[data-sender]').attr('disabled', 'disabled');
        } else {
            console.log('a');
            $('[data-msg]').text('');
            $('[data-sender]').removeAttr('disabled');
        }
    });

    $(document).on('click', '[data-sender]', function (e) {
        e.preventDefault();
        let firtsName = $('[data-first_name]');
        let lastName = $('[data-last_name]');
        let street = $('[data-street]');
        let city = $('[data-city]');
        let msg = '';
        let error = 0;
        regexp = /^[A-Z]{1}/;
        if (!regexp.test(firtsName.val())) {
            firtsName.addClass('error');
            msg = msg + 'Imię powinno być z dużej litery <br />';
            error++;
        }
        if (!regexp.test(lastName.val())) {
            lastName.addClass('error');
            msg = msg + 'Nazwisko powinno być z dużej litery <br />';
            error++;
        }
        if (!regexp.test(street.val())) {
            street.addClass('error');
            msg = msg + 'Ulica powinno być z dużej litery';
            error++;
        }
        if (!regexp.test(city.val())) {
            city.addClass('error');
            msg = msg + 'Miasto powinno być z dużej litery';
            error++;
        }
        $('[data-msg]').html(msg);
        if (error == 0) {
            $('form').submit();
        }
    })


}

$(document).ready(function () {
    formValidate();
});